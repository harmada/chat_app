import { server } from "../api/serverUrl"
import axios from "axios";
import useSWR from 'swr'



export const follow = async (data) => {
  const result = await axios.post(`${server}/follow`, data);
  return result;
}

export const isFollow = async (payload) => {
  const result = await axios.post(`${server}/isfollow`, payload);
  const { data } = result.data;
  return data;
}

export const unfollow = async (payload) => {
  const result = await axios.put(`${server}/unfollow`, payload)
  return result;
}

export const followers = async (data) => {
  const payload = { 
    _id: data
  }
  const result = await axios.post(`${server}/followers`, payload);
  return result

}
import axios from 'axios';
import { server } from '../api/serverUrl'
import { getCompanyProfile } from '../hooks/authHooks'

export const searchCompanyProfile = async  ({company})  => {
  const companyName = await axios.post(`${server}/companyname`, { id: company });
  return  companyName.data.company;
}

export const updateProfile = async (profile) => {
  const company = await getCompanyProfile(profile)
  profile.company = company
  const result = await axios.put(`${server}/profile`, profile)
  return result;
}

export const getCompanyDetails = async(userDetails) => {
  const getCompany = await searchCompanyProfile(userDetails);
  const { company } = getCompany;
  const payload = {
    company: company,
  }
  return payload;
}

export const uploadProfilePicture = async (profilepicture) => {
  const profile = await axios.post(`${server}/profilepicture`, profilepicture)
  return profile;
}

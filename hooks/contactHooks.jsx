
import { useState } from 'react';
import Colors from '../constants/Colors';
import axios from 'axios';
import { server } from '../api/serverUrl';

export const userStatus = async (status) => {
  switch (status) {
    case 'Online':
      return Colors.status.online;
    case 'Offline':
      return Colors.status.offline;
    case 'Busy':
      return Colors.status.buzy;
  }
}

export const searchUser = async (text) => {
  const user = {
    user: text
  }
  const users = await axios.post(`${server}/user`, user);
  return users;
} 


import * as RootNavigation from '../RootNavigation';
import { Alert } from "react-native";
import { server } from "../api/serverUrl"
import axios from "axios";
import * as SecureStore from 'expo-secure-store';

export const loginAuth = async (credentials) => {
  try {
    const result = await axios.post(`${server}/login`, credentials)
    const { token, user } = result.data;
    SecureStore.setItemAsync('token', token)
    return user;
  }
  catch ({ response }) {
    const { data } = response;
    Alert.alert("Error", data.error)
  }
}

export const getDataFromToken = async () => {
  const token = await SecureStore.getItemAsync('token');
  try {
    const user = await axios.post(`${server}/me`, { token }, { headers: { "Authorization": `${token}` } })
    const { data } = user;
    const { userProfile } = data;
    
    if(typeof userProfile === 'object') {
      return userProfile;
    }
  }
  catch ({ response }) {
    console.log('response', response.error)
  }
} 

export const register = async (credentials) => {
  axios.post(`${server}/signup`, credentials )
    .then(result => {
      const { data } = result;
      Alert.alert(data.header, data.message)
    })
    .catch(({ response }) => {
      const { data } = response

      Alert.alert("Error", data.error)
    })
}

export const getCompanyProfile = async (credentials) => {

  const { company } = credentials;
  const companyDetails = await axios.get(`${server}/company`, { params: { company }});
  if( companyDetails.data.status === 404 ) {
    const comp = { company, description: "Company" }
    const companyProfile = await axios.post(`${server}/company`, comp)
    return companyProfile.data.result
  }
  return companyDetails.data.company;
}


export const updateAccount = async (account) => {
  const token = await SecureStore.getItemAsync('token');
  const result = await axios.post(`${server}/account`, account , { headers: { "Authorization": `${token}` }})
  return result;
}

export const registerToken = async (expoToken) => {
  const result = await axios.post(`${server}/token`, expoToken)
  return result
}

export const checkToken = async (userId) => {
  const result = await axios.get(`${server}/token?userId=${userId}`)
  return result
}

export const forgotPassword = async (email) => {
  try {
    const result = await axios.post(`${server}/forgotpassword`, email)
    return result;
  }
  catch ({ response }) {
    Alert.alert(response.data.message)
  }
}
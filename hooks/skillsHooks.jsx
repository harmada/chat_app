import axios from 'axios'
import { server } from '../api/serverUrl';

export const searchIssuer = async (e) => {
  try {
    const result = axios.get(`https://autocomplete.clearbit.com/v1/companies/suggest?query=${e}`)
    return result;
  } 
  catch (e) {
    console.error(e)
  }
}

export const createSkills = async (skills) => {
  const result = await axios.post(`${server}/skill`, skills);
  return result;
}

export const getSkills = async ({ _id }) => {
  try {
    const user_id = _id;
    const result = await axios.get(`${server}/skill`, { params: { user_id } })
    return result.data;
  } 
  catch (e) {
    console.error(e)
  }

}
import { server } from '../api/serverUrl';
import axios from 'axios';

export const sendChat = async (message) => {
	const result = await axios.post(`${server}/chat`, message )
	return result;
}

export const getChatMessages = async (chatRoom) => {
	const result = await axios.get(`${server}/chatmessage?pageNum=${chatRoom.pageNum}&chatRoomId=${chatRoom.chatRoomId}`)
	return result;
}
import { Alert } from "react-native";
import { server } from "../api/serverUrl"
import axios from "axios";

export const createChatRoom = async (data) => {
  const result = await axios.post(`${server}/chatroom`, data);
  return result;
}

export const getChatId = async (data) => {
  const result = await axios.post(`${server}/chatid`, data);
  return result;
}
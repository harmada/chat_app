import React from 'react'
import { View, Text } from 'react-native'
import ConnectionsProfile from '../components/ConnectionsProfile';
import ChatHeader from '../components/ChatHeader';

const ConnectionsScreen = (props) => {

  return (
    <View>
      <ChatHeader userDetails={props}/>
      <ConnectionsProfile userDetails={props}/>
    </View>
  )
}
export default ConnectionsScreen
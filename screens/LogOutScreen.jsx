import React, { useContext } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window')
import { useNavigation } from '@react-navigation/native';
import { UserContext } from '../context/UserContext';
import * as SecureStore from 'expo-secure-store';
import { ContactContext } from '../context/ContactContext'

const LogOutScreen = () => {

  const [,userDispatch] = useContext(UserContext);
  const [,contactDispatch] = useContext(ContactContext)


  const navigation = useNavigation();
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.main.main,
        height,
        width,
      }}
    >
      <Text
        style={{
          fontSize: height * .04,
          color: Colors.main.background,
        }}
      >
        Do you want to logout?
      </Text>
      <View
        style={{
          flexDirection: 'row',
          padding: 20,
        }}
      >
        <TouchableOpacity
          style={{
            backgroundColor: Colors.main.danger,
            borderRadius: 10,
            margin: 10,
          }}
          onPress={()=> navigation.goBack()}
        >
          <Text
            style={{
              fontSize: height * .02,
              fontWeight: 'bold',
              alignSelf: 'center',
              color: Colors.main.background,
              padding: 10,
              width: width * .3
            }}
          >
            No
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: Colors.main.dirtyWhite,
            borderRadius: 10,
            margin: 10,
          }}
          onPress={()=> {
            userDispatch({ type: 'DELETE_DATA' });
            contactDispatch({ type: 'RESET_DATA' });
            navigation.navigate('Auth');
            SecureStore.deleteItemAsync("token");
          }}
        >
          <Text
            style={{
              fontSize: height * .02,
              color: Colors.main.main,
              padding: 10,
              width: width * .3
            }}
          >
            Yes
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default LogOutScreen

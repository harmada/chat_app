import React, { useState, useEffect, useContext } from 'react'
import { ScrollView,View ,Text, FlatList, TouchableOpacity, Image, Dimensions  } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import styles from '../styles/profile';
import Colors from '../constants/Colors';
import Profile from '../components/Profile';
import Account from '../components/Account';
import { UserContext } from '../context/UserContext';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { RNS3 } from 'react-native-s3-upload';
import { uploadProfilePicture }  from '../hooks/profileHooks';
const { width, height } = Dimensions.get('window')
import { ACCESS_KEY, SECRET_KEY } from '../env.js';


const ProfileScreen = () => {   
  const [userState, ] = useContext(UserContext);
  const [tabState, setTabState] = useState(1);
  const [image, setImage] = useState(userState.image);


  useEffect(() => {
    getImage();
  }, []);

  const getImage = async () => {
    if (Platform.OS !== 'web') {
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri)
      const file = {
        uri: result.uri,
        name: `${userState._id}.jpg`,
        type: "image/jpg"
      }

      const options = {
        keyPrefix: "profile_picture/",
        bucket: "officecommunicator",
        region: "ap-southeast-1",
        accessKey: `${ACCESS_KEY}`,
        secretKey: `${SECRET_KEY}`,
        successActionStatus: 201
      }

      RNS3.put(file, options)
        .then(async response => {
          if (response.status !== 201) {
            throw new Error("Failed to upload image to S3");
          }
          const payload = {
            _id: userState._id,
            profilePicture: response.body.postResponse.location
          }
          const imageDetails = await uploadProfilePicture(payload)
        })
        .catch(error => {
          console.log('error', error)
        });
    }
  };

  return (

    <ScrollView
      style={{
        backgroundColor: Colors.main.dirtyWhite,
      }}
    >
      <View
        style={{
          flexDirection: 'column',
        }}
      >
        <TouchableOpacity
          onPress={() => pickImage()}
        >
        { 
          image
            ? <Image 
                style={{ 
                  height: height * .45,
                  height: height * .4,
                  width: width,
                }} 
                source={{ uri: image }} 
              />
            : <Image 
                style={{
                  height: height * .4,
                  width: width,

                }} 
                source={ require('../assets/male-placeholder.png') } 
              /> 
          }
        </TouchableOpacity>
      </View>
      <View
        style={{ alignItems: 'center',}}
      >
        <View
          style={{
            flexDirection: 'row',
            padding: 20,
            justifyContent: 'space-between'
          }}
        >
          <TouchableOpacity
            style={[{
              height: height * .05,
              width: width * .4,
              padding: 10,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              margin: 2,
              elevation: 2,
              shadowOpacity: 0.5,
              shadowOffset: { height: 5, width: -1 },
              shadowColor: Colors.main.main
            },
            {
              backgroundColor: tabState === 2 ? Colors.main.lines : Colors.main.main,
            }]}
            onPress={()=> setTabState(1)}
          >
            <Text
              style={{
                color: tabState === 2 ? Colors.main.main : Colors.main.lines,
              }}
            >
              Profile
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[{
              height: height * .05,
              width: width * .4,
              padding: 10,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              margin: 2,
              elevation: 2,
              shadowOpacity: 0.5,
              shadowOffset: { height: 5, width: -1 },
              shadowColor: Colors.main.main
            },
            {
              backgroundColor: tabState === 1 ? Colors.main.lines : Colors.main.main,
            }]}
            onPress={()=> setTabState(2)}
          >
            <Text
              style={{
                color: tabState === 1 ? Colors.main.main : Colors.main.lines,
              }}
            >
              Account
            </Text>
          </TouchableOpacity>

        </View>
        
      </View>
      {/* <View 
        style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.main.main }}
      > */}
{/*     
              <TouchableOpacity
                style={[styles.tabStyle && { borderWidth: 1, borderColor: '#fff' } ]}
                // onPress={()=> setTabState(item)}
              >
                <MaterialCommunityIcons name={ICON} size={24} color="white" />
                <Text
                  style={{ color: 'white'}}
                >
                  {item.tab}
                </Text>
              </TouchableOpacity>  */}
       
      {/* </View> */}
      <View>
        { tabState === 1 && <Profile/> }  
        { tabState === 2 && <Account/> }  
      </View>
    </ScrollView>

  )
}

export default ProfileScreen;
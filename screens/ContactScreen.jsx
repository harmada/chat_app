import React, { useState, useEffect, useContext } from 'react'
import { View, FlatList, Dimensions } from 'react-native';
import ContactList from '../components/ContactsList';
import SearchContacts from '../components/SearchContacts';
import { followers } from '../hooks/followHooks';
import { UserContext } from '../context/UserContext';
import * as Progress from 'react-native-progress';
import { ContactContext } from '../context/ContactContext';
import Colors from '../constants/Colors'
const { width, height } = Dimensions.get('window');

const ContactScreen = () => {   

  const [contactState, contactDispatch] = useContext(ContactContext);
  const [userState,] = useContext(UserContext);
  const [textLength, setTextLength ] = useState();

  const [users, setUsers] = useState(Object.values(contactState))
  const [loading, setLoading] = useState()

  useEffect(() => {
    getFollowers()
  },[])

  const getFollowers = async () => {
    setLoading(true)
    setTimeout(() => {
      setUsers(contactState);
      setLoading(false)
    },1000)
  }
  
  const setUserSearch = async (followers) => {
    if(!textLength) {
      return getFollowers();
    }
    setUsers(followers);
  }

 

  return (
    <View
      style={{
        marginTop: 5,
        backgroundColor: '#e5e5e5',
        height
      }}
    >
      <SearchContacts getUser={setUserSearch} textLength={setTextLength} />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {
            loading 
            ? <Progress.Circle size={30} indeterminate={true} color={Colors.main.main} />
            : <FlatList
              data={ users.length === 0 ? contactState : users }
              renderItem={({ item }) => <ContactList user={item} />}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item) => item._id}
            /> 
          }
          
        </View>
       
    </View>
  )
}

export default ContactScreen;
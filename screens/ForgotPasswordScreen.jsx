import React, { useState } from 'react'
import { ScrollView, View, TouchableOpacity, Text, TextInput, Dimensions, Image, Alert } from 'react-native';
const { height, width } = Dimensions.get('window')
import Colors from '../constants/Colors';
import * as RootNavigation from '../RootNavigation';
import validator from 'validator';
import { forgotPassword } from '../hooks/authHooks'

const ForgotPasswordScreen = () => {
  const [error, setError] = useState(false);
  const [email, setEmail] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const resetPassword = async () => {
    if(email.length === 0) return setError(true)
    setError(!validator.isEmail(email))
    const payload = {
      email,
    }

    const result = await forgotPassword(payload)
    if(result.status === 200) {
      Alert.alert("Hi, Kindly Check your email for password reset")
    }
  }

  return (
    <View 
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        height,
        width,
        backgroundColor: Colors.main.dirtyWhite
      }}
    >
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'space-around',
        }}
      >
        <Text
          style={{
            color: Colors.main.main,
            fontWeight: 'bold',
            marginVertical: 10,
            fontSize: 15
          }}
        >
          Forgot Password
        </Text>
        <TextInput
          placeholderTextColor={Colors.main.main}
          placeholder={'Email'}
          autoCapitalize="none"
          autoCorrect={false}
          style={{
            backgroundColor: Colors.main.background,
            height: height * .07,
            padding: 10,
            width: width * .8,
            marginHorizontal: 10,
            borderRadius: 10,
            elevation: 6,
            shadowOpacity: .8,
            marginVertical: 10,
            shadowOffset: { height: 5, width: - 2 }
          }}
          onChangeText={setEmail}
        />
        {
          error && (<Text style={{ color: 'red' }}> Email is not valid</Text>)
        }
        <TouchableOpacity
          style={{
            backgroundColor: Colors.main.main,
            height: height * .05,
            borderRadius: 10,
            width: width * .7,
            padding: 10,
            marginVertical: 20,
            elevation: 6,
            shadowOpacity: .8,
            shadowOffset: { height: 5, width: - 2 },
            alignItems: 'center',
            justifyContent: 'center'
          }}
          onPress={() => {
            resetPassword();
          }}
        >
          <Text
            style={{
              color: Colors.main.background,
              fontSize: 20
            }}
          >
            Reset Password 
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: height * .06,
            borderRadius: 10,
            width: width * .7,
            padding: 10,
            marginVertical: 20,
            alignItems: 'center',
            justifyContent: 'center'
          }}
          onPress={()=> {
            RootNavigation.navigate('Auth')
          }}
        >
        <Text
          style={{
            color: Colors.main.main,
            fontSize: 20
          }}
        >
          Back to Login
        </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default ForgotPasswordScreen;
 
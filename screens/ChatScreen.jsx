import React from 'react'
import { View, Image, Text } from 'react-native';
import styles from '../styles/chat';


const ChatScreen = (props) => {
  const { imageUri } = props;

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: imageUri }} />
      <Text>
      </Text>
    </View>
  )
}

export default ChatScreen;
import React, { useEffect, useContext } from 'react';
import AuthCard from '../components/Auth';
import * as SecureStore from 'expo-secure-store';
import { getDataFromToken } from '../hooks/authHooks';
import { UserContext } from '../context/UserContext'; 

const AuthScreen = ({ navigation }) => { 

  const [,dispatch] = useContext(UserContext)

  useEffect(()=> {
    getToken();
  },[])

  const getToken = async () => {
    const token = await SecureStore.getItemAsync("token");
    const userDetails = await getDataFromToken(token);
    dispatch({type: "SAVE_DATA", payload: userDetails})
    userDetails && navigation.navigate("Home")
  }

  return (
    <AuthCard />
  )
}
 
export default AuthScreen; 
import React, { useState, useContext, useEffect } from 'react'
import { View, Text, FlatList, ScrollView, Dimensions, KeyboardAvoidingView } from 'react-native';
import styles from '../styles/chatMessage';
import InputTextMessage from '../components/InputTextMessage';
import ChatMessage from '../components/ChatMessage';
import ChatHeader from '../components/ChatHeader';
import { UserContext } from '../context/UserContext';
import { sendChat, getChatMessages } from '../hooks/messageHooks';
import { io } from "socket.io-client";
import { server } from "../api/serverUrl"
import Colors from '../constants/Colors';
import * as Progress from 'react-native-progress';
const { width, height } = Dimensions.get('window')

const ChatMessageScreen = (props) => {   

  const socket = io(`${server}`, {
    cors: { origin: "*" },
    transports: ["websocket"],
  })

  const [receiverData, setReceiverData] = useState(props.route.params)
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(1);
  const [messages, setMessages] = useState([]);
  const [pageLimit, setPageLimit] = useState()

  useEffect(() => {
    getMessage(page);
    socketMessage();
    return () => {
      socket.off(`${receiverData.chatRoomId}`);
    }
  },[])

  const socketMessage = () => {
    socket.on(`${receiverData.chatRoomId}`,() => {
      getMessage();
    })
  }

  const getMessageData = async (pageNum) => {
    const payload = {
      chatRoomId: receiverData.chatRoomId,
      pageNum: pageNum >= pageLimit ? pageNum : page
    }
    setLoading(true);
    const chats = await getChatMessages(payload)
    const { data } = chats
    setTimeout(() => {
      setLoading(false);
    }, 1000)
    return data
  }

  const getMessage = async () => {
    setPage(1)
    const chats = await getMessageData(1)
    const { chat } = chats
    setMessages(chat)
  }

  const getMoreMessage = async (pageNum) => {
    const chats = await getMessageData(pageNum);
    setPageInfo(chats, pageNum)
  }

  const setPageInfo = async ({ pageCount, chat }, page) => {
    console.log(pageCount)
    setPage(page)
    setPageLimit(pageCount)
    const combineOldAndNew = messages.concat(chat);
    const newChatList = [...new Map(combineOldAndNew.map(item => [JSON.stringify(item), item])).values()]
    setMessages(newChatList)
  }

  const handleLoadMoreMessage = (pageNum) => {
    console.log('page',page)
    console.log('pageLimit',pageLimit)
    console.log('pageNum',pageNum)
    setPage(pageNum >= pageLimit ? page : pageNum) 
    pageNum >= pageLimit ? console.log('stop',pageNum) : getMoreMessage(pageNum);
  }

  const messageInput = async (content) => {
    const chatPayload = {
      chatRoomId: receiverData.chatRoomId,
      userId: receiverData._id,
      content: content
    }
    await sendChat(chatPayload)
  }

  const isIOS = Platform.OS === 'ios';

  const behavior = Platform.OS === "ios" ? "padding" : "";
  return (
    <KeyboardAvoidingView
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
      behavior={behavior}
    >
      <View>
        <FlatList
          inverted
          showsVerticalScrollIndicator={false}
          data={messages}
          renderItem={({item})=> <ChatMessage message={item} />}
          keyExtractor={(item => item._id)}
          onEndReached={() => { handleLoadMoreMessage(page + 1) }}
          onEndReachedThreshold={isIOS ? 0 : 1}
        />
        <InputTextMessage inputMessage={messageInput} />
      </View>
    </KeyboardAvoidingView>
  )
}

export default ChatMessageScreen;
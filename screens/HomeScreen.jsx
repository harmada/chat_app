import React, { useContext, useEffect,  useRef } from 'react';
import { Text, View } from 'react-native';
import styles from '../styles/home';
import * as SecureStore from 'expo-secure-store';
import { UserContext } from '../context/UserContext';
import CardProfile from '../components/CardProfile';
import CardBodyHome from '../components/CardBodyHome';
import { getDataFromToken, registerToken, checkToken } from '../hooks/authHooks';
import { followers } from '../hooks/followHooks';
import { searchCompanyProfile, getCompanyDetails } from '../hooks/profileHooks';
import { CompanyContext } from '../context/CompanyContext';
import { ContactContext } from '../context/ContactContext';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';

const HomeScreen = () => {
  const notificationListener = useRef();
  const responseListener = useRef();

  const [userState,userDispatch] = useContext(UserContext);
  const [,companyDispatch] = useContext(CompanyContext);
  const [,contactDispatch] = useContext(ContactContext);

  useEffect(() => {
    checkExpoToken();
    getUserDetails();
  },[])


  const checkExpoToken = async () => {
    const result =  await checkToken(userState._id);
    if (result.data.result.length === 0) {
      return generateExpoToken();
    }
  }

  const generateExpoToken = () => {
    registerForPushNotificationsAsync()
    .then(async (token) => {
      const payload = {
        expoToken: token,
        userId: userState._id
      }
      const result = await registerToken(payload);
    });
    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log(response);
    });
  }

  const registerForPushNotificationsAsync = async () => {

    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      console.log(token);
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

    console.log('token', token)
    return token;
  }

  const getUserDetails = async () => {
    const userDetails = await getDataFromToken();
    const company = await getCompanyDetails(userDetails)
    const contacts = await followers(userState._id);

    companyDispatch({type: 'SAVE_DATA', payload: company });
    userDispatch({ type: 'SAVE_DATA', payload: userDetails })
    contactDispatch({type: 'SAVE_DATA', payload: contacts.data.followers });
  }

  return (
    <View style={styles.container}>
      <CardProfile/>
      <CardBodyHome />
    </View>
  );
} 

export default HomeScreen;


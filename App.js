import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Navigations from './navigations';
import { UserProvider } from './context/UserContext';
import { CompanyProvider } from './context/CompanyContext';
import { ContactProvider } from './context/ContactContext';


function App() {
  return (
    <SafeAreaProvider>
      <UserProvider>
        <ContactProvider>
          <CompanyProvider>
            <Navigations/>
            <StatusBar />
          </CompanyProvider>
        </ContactProvider>
      </UserProvider>
    </SafeAreaProvider>
  );
}

export default App;



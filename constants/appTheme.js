import { Dimensions } from 'react-native';
import { ms, mvs } from 'react-native-size-matters';

const { width, height } = Dimensions.get('window');

export const COLORS = {
  // BASE
  PRIMARY: '#CA9439',

  // COLORS
  WHITE: '#FFFFFF',
  BLACK: '#1E1F20',
  TRANSPARENT: 'transparent',
  TEXT: '#323232',

  LIGHT_GRAY: '#F5F5F6',
  LIGHT_GRAY2: '#F6F6F7',
  LIGHT_GRAY3: '#EFEFF1',
  LIGHT_GRAY4: '#F8F8F9',
  DARK_GRAY: '#898C95',
  WARM_GREY: '#7C7C7C',

  INPUT_BACKGROUND_COLOR: '#F6F7FA',
  BORDER_COLOR: '#E3E3E3',
};

export const SIZES = {
  // GLOBAL SIZES
  BASE: mvs(8),
  FONT: mvs(14),
  RADIUS_SMALL: 8,
  RADIUS_BIG: 16,
  PADDING_HORIZONTAL: ms(20),
  PADDING_VERTICAL: mvs(20),

  // FONT SIZES
  h1: mvs(30),
  h2: mvs(22),
  h3: mvs(20),
  h4: mvs(18),
  h5: mvs(16),
  body1: mvs(30),
  body2: mvs(20),
  body3: mvs(16),
  body4: mvs(14),
  body5: mvs(12),

  // APP_DIMENSION
  width,
  height,
};

export const FONTS = {
  h1: {
    fontFamily: 'roboto-medium',
    fontSize: SIZES.h1,
    lineHeight: mvs(40),
    color: COLORS.TEXT,
  },
  h2: {
    fontFamily: 'roboto-medium',
    fontSize: SIZES.h2,
    lineHeight: mvs(36),
    color: COLORS.TEXT,
  },
  h3: {
    fontFamily: 'roboto-medium',
    fontSize: SIZES.h3,
    lineHeight: mvs(30),
    color: COLORS.TEXT,
  },
  h4: {
    fontFamily: 'roboto-medium',
    fontSize: SIZES.h4,
    lineHeight: mvs(22),
    color: COLORS.TEXT,
  },
  h5: {
    fontFamily: 'roboto-medium',
    fontSize: SIZES.h5,
    lineHeight: mvs(22),
    color: COLORS.TEXT,
  },

  h1_styled: {
    fontFamily: 'philo-bold',
    fontSize: SIZES.h1,
    lineHeight: mvs(40),
    color: COLORS.TEXT,
  },
  h2_styled: {
    fontFamily: 'philo-bold',
    fontSize: SIZES.h2,
    lineHeight: mvs(36),
    color: COLORS.TEXT,
  },
  h3_styled: {
    fontFamily: 'philo-bold',
    fontSize: SIZES.h3,
    lineHeight: mvs(30),
    color: COLORS.TEXT,
  },
  h4_styled: {
    fontFamily: 'philo-bold',
    fontSize: SIZES.h4,
    lineHeight: mvs(22),
    color: COLORS.TEXT,
  },
  h5_styled: {
    fontFamily: 'philo-bold',
    fontSize: SIZES.h5,
    lineHeight: mvs(22),
    color: COLORS.TEXT,
  },

  body1: {
    fontFamily: 'roboto-regular',
    fontSize: SIZES.body1,
    lineHeight: mvs(36),
    color: COLORS.WARM_GREY,
  },
  body2: {
    fontFamily: 'roboto-regular',
    fontSize: SIZES.body2,
    lineHeight: mvs(30),
    color: COLORS.WARM_GREY,
  },
  body3: {
    fontFamily: 'roboto-regular',
    fontSize: SIZES.body3,
    lineHeight: mvs(22),
    color: COLORS.WARM_GREY,
  },
  body4: {
    fontFamily: 'roboto-regular',
    fontSize: SIZES.body4,
    lineHeight: mvs(22),
    color: COLORS.WARM_GREY,
  },
  body5: {
    fontFamily: 'roboto-regular',
    fontSize: SIZES.body5,
    lineHeight: mvs(22),
    color: COLORS.WARM_GREY,
  },

  body1_styled: {
    fontFamily: 'philo-regular',
    fontSize: SIZES.body1,
    lineHeight: mvs(36),
    color: COLORS.WARM_GREY,
  },
  body2_styled: {
    fontFamily: 'philo-regular',
    fontSize: SIZES.body2,
    lineHeight: mvs(30),
    color: COLORS.WARM_GREY,
  },
  body3_styled: {
    fontFamily: 'philo-regular',
    fontSize: SIZES.body3,
    lineHeight: mvs(22),
    color: COLORS.WARM_GREY,
  },
  body4_styled: {
    fontFamily: 'philo-regular',
    fontSize: SIZES.body4,
    lineHeight: mvs(22),
    color: COLORS.WARM_GREY,
  },
  body5_styled: {
    fontFamily: 'philo-regular',
    fontSize: SIZES.body5,
    lineHeight: mvs(22),
    color: COLORS.WARM_GREY,
  },
};

const appTheme = { COLORS, FONTS, SIZES };
export default appTheme;

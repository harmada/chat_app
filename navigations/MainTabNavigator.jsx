import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import HomeScreen from '../screens/HomeScreen';
import ChatScreen from '../screens/ChatScreen';
import ContactScreen from '../screens/ContactScreen';
import Colors from '../constants/Colors';
const MainStack = createMaterialTopTabNavigator();
import { Entypo, MaterialCommunityIcons, AntDesign, EvilIcons } from '@expo/vector-icons'; 
import useColorScheme from '../hooks/colorScheme';
import { navigationRef } from '../RootNavigation';


const MainTabNavigator = () => {

  const colorScheme = useColorScheme();
  return (
      <MainStack.Navigator
        ref={navigationRef}
        tabBarOptions={{
          style: {
            backgroundColor: Colors.main.main,
          },
          indicatorStyle: {
            backgroundColor: Colors.light.background,
            height: 5
          },
          labelStyle: {
            fontWeight: 'bold',
          },
          activeTintColor: Colors[colorScheme].background,
          showIcon: true
        }}
      >
        <MainStack.Screen 
          name="Home" 
          component={HomeScreen}
          options={{
            tabBarIcon: () => (
              <AntDesign name="home" size={24} color="white" />
            ),
          }}
        />
    
        <MainStack.Screen 
          name="Contact" 
          component={ContactScreen}
          options={{
            tabBarIcon: () => (
              <AntDesign name="contacts" size={24} color="white" />
            ),
          }}
        />
        {/* <MainStack.Screen 
          name="Chat" 
          component={ChatScreen}
          options={{
            tabBarIcon: () => (
              <AntDesign name="wechat" size={24} color="white" />
            ),
            title: "Chat",
            headerStyle: {
              backgroundColor: '#f4511e',
            },
          }}
        /> */}
     
    </MainStack.Navigator>
  )
}

export default MainTabNavigator;
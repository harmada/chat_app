import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { TouchableOpacity, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import MainTabNavigator from './MainTabNavigator';
import ProfileScreen from '../screens/ProfileScreen';
import AuthScreen from '../screens/AuthScreen';
import RegisterScreen from '../screens/RegisterScreen';
import ChatMessageScreen from '../screens/ChatMessageScreen';
import ConnectionsScreen from '../screens/ConnectionsScreen';
import LogOutScreen from '../screens/LogOutScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';

import { MaterialCommunityIcons, MaterialIcons, EvilIcons } from '@expo/vector-icons'; 
import Colors from '../constants/Colors';
import navStyles from '../styles/navigation';
import { navigationRef } from '../RootNavigation';
import * as RootNavigation from '../RootNavigation';

const RootStack = createStackNavigator();

const Navigations = () => {

  return (
    <NavigationContainer ref={navigationRef}>
      <RootStack.Navigator
        initialRouteName="Auth"
        screenOptions={{
          headerStyle: { 
            backgroundColor: Colors.main.main,
            shadowOpacity: 0, 
            elevation: 0
          },   
          headerTintColor: Colors.light.background,
          headerTitleStyle: {
            fontWeight: 'bold'
          },
          showIcon: true
        }}      
      >
        <RootStack.Screen
          name="Home" 
          component={MainTabNavigator}
          options={{
            title: "Office Communicator",
            headerLeft: () => (
              <MaterialCommunityIcons name="office-building" size={24} color="white" />
            ),
            headerRight: () => (
              <View style={navStyles.iconContainer}>
                <TouchableOpacity
                  style={navStyles.icon}
                  onPress={() => {
                    RootNavigation.navigate('Profile');
                  }}
                >
                  <EvilIcons name="user" size={29} color="white" />
                </TouchableOpacity>
                <TouchableOpacity 
                  style={navStyles.icon}
                  onPress={() => {
                    RootNavigation.navigate('Logout')
                  }}
                >
                <MaterialIcons name="logout" size={24} color="white" />
                </TouchableOpacity>
              </View>
            )
          }}
          
        />
        <RootStack.Screen 
          name="Profile" 
          component={ProfileScreen} 
          options={{ 
              headerTransparent: true,
              headerShown: true,
          }}
        />
        <RootStack.Screen 
          name="Logout" 
          component={LogOutScreen}
          options={{ 
            header: () => {null}
          }}
        />
        <RootStack.Screen 
          name="ChatMessage" 
          component={ChatMessageScreen}
          options={(props)=>({
            title: props.route.params.name,
          })}
        />
        <RootStack.Screen 
          name="ForgotPassword" 
          component={ForgotPasswordScreen}
          options={{ 
            header: () => {null}
          }}
        />
        <RootStack.Screen
          name="Auth"
          component={AuthScreen}
          options={{
            header: () => { null }
          }}
        />
        <RootStack.Screen 
          name="Register" 
          component={RegisterScreen}
          options={()=>({
            header: () => { null } 
          })}
        />
        <RootStack.Screen 
          name="Connection" 
          component={ConnectionsScreen}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  )
}

export default Navigations;
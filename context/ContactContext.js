import React, { createContext, useReducer} from "react";

export const ContactContext = createContext("");

const initialState = []

const reducer = (state, action) => {
  switch (action.type) {
    case "SAVE_DATA":
      const contacts = action.payload.concat(state);
      const filteredContacts = [...new Map(contacts.map(item => [JSON.stringify(item), item])).values()]
      return filteredContacts;
    case 'ADD_DATA':
      return state.push(action.payload)
    case 'DELETE_DATA':
      const newContacts = state.filter(contacts => contacts._id !== action.payload)  
      return newContacts;
    case 'RESET_DATA': 
      return initialState
    default:
      return state;
  }
}


export const ContactProvider = props => {

  const [state, dispatch] = useReducer(reducer, initialState)
  return (
    <ContactContext.Provider value={[state, dispatch]}>
      {props.children}
    </ContactContext.Provider>
  )
 
}


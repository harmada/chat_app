# [Office Communicator](https://gitlab.com/harmada/chat_app#) 


Office Communicator is an application made for Professionals to connect with there collegues. This application was made via, [React Native](https://facebook.github.io/react-native/?ref=creativetim) and [Expo](https://expo.io/?ref=creativetim) for Frontend and [Node JS]() and [MongoDB] for Backend.

### Technology use to make this app possible
* [ReactNative]
* [NodeJS]
* [MongoDB]
* [Socket.IO]
* [Mongoose]
* [Expo]
* [AWS]
* [EC2]
* [S3]


# Components
[Login](https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.37.39.png)<img src="https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.37.39.png"> |
[HomeScreen](https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.37.54.png)<img src="https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.37.54.png">
[Contact](https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.01.png)<img src="https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.01.png">
[Search](https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.09.png)<img src="https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.09.png">
[ConnectionProfile](https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.13.png)<img src="https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.13.png">
[Chat](https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.22.png)<img src="https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+12+Pro+Max+-+2021-07-28+at+12.38.22.png">
[EditProfile](https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+8+-+2021-07-28+at+16.25.38.png) <img src="https://officecommunicator.s3.ap-southeast-1.amazonaws.com/chat_app/Simulator+Screen+Shot+-+iPhone+8+-+2021-07-28+at+16.25.38.png">







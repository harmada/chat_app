import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, Dimensions } from 'react-native'
import { getSkills } from '../hooks/skillsHooks';
import SkillList from '../components/Skills';
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window')

const ConnectionsProfile = ({ userDetails }) => {

  const [skillList, setSkillList] = useState([]);
  const { params } = userDetails.route;


  const fetchSkills = async () => {
    const payload = {
      _id: params._id, 
    }
    const skillListArray = await getSkills(payload);
    const { result } = skillListArray;
    if(skillListArray.result.length === 0) return null;
    setSkillList(result)
  }

  useEffect(() => {
    fetchSkills();
  },[])

  return (
    <View
      style={{
        padding: 20,
        margin: 10
      }}
    >
      <Text
        style={{
          fontSize: 15,
          fontWeight: 'bold',
          color: Colors.main.main,
          marginBottom: width * .02
        }}
      >
        Skills and Certificates
      </Text>
      <FlatList
        keyExtractor={(item => item._id)}
        showsVerticalScrollIndicator={false}
        data={skillList}
        renderItem={({ item })=> {
          return <SkillList skills={item} />
        }}
      />
    </View>
  )
}

export default ConnectionsProfile

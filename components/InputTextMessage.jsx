import React, { useState, useEffect } from 'react';
import { View, TextInput, TouchableOpacity } from 'react-native';
import styles from '../styles/inputTextMessage'
import { MaterialIcons, Ionicons, AntDesign, Entypo, Fontisto, MaterialCommunityIcons } from '@expo/vector-icons'; 
import { useDebounce } from 'use-debounce';


const InputTextMessage = (props) => {


  const { inputMessage } = props;
  const [message, setMessage] = useState('')
  const [value] = useDebounce(message, 800, { leading: true });


  const onPress = () => {
    inputMessage(message)
    setMessage("")
  }

  useEffect(() => {
  },[value])

  return (
    <View style={styles.container}>
      <View style={styles.mainContainer}>
      <AntDesign name="message1" size={24} color="black" />
        <TextInput 
          style={styles.textInput}
          multiline
          value={message}
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={setMessage}
        />
        { !message && <Fontisto name="camera" size={24} color="grey"/> }
        <TouchableOpacity>
          <Entypo style={styles.icon} name="attachment" size={24} color="grey"/>
        </TouchableOpacity>
      </View>
      <View  style={styles.buttonContainer}> 
        <TouchableOpacity
          onPress={onPress}
        >
        { !message 
          ? <MaterialCommunityIcons name='microphone' size={24} color="white"/> 
          : <MaterialIcons name="send" size={24} color="white"/> 
        }
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default InputTextMessage;
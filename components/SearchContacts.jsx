import React, { useState, useEffect, useContext } from 'react'
import { View, TextInput, Dimensions } from 'react-native';
import styles from '../styles/contact';
import { Ionicons } from '@expo/vector-icons'; 
import { searchUser } from '../hooks/contactHooks';
import { useDebounce } from 'use-debounce';
const { width, height } = Dimensions.get('window');
import Colors from '../constants/Colors';



const SearchContacts = (props) => {
  const { getUser, textLength }  = props;
  const [ searchData, setSearchData] = useState();

  const [value] = useDebounce(searchData, 1000, { leading: true });

  useEffect(() => {
    fireApi();
  },[value])

  const fireApi = async () => {
    const users = await searchUser(searchData);
    return getUser(users.data.result)
  }

  return (
    <View style={{
      backgroundColor: 'white',
      flexDirection: 'row',
      height: height * .07,
      marginHorizontal: 10,
      marginVertical: 5,
      fontSize: 10,
      borderRadius: 10,
      alignItems: 'center'
      
    }}>
      <Ionicons 
        style={{
          paddingHorizontal: 5,
          marginHorizontal: 5
        }} 
        name="ios-search" 
        size={25} 
        color={Colors.main.main} 
      />
        <TextInput
          placeholderTextColor={Colors.main.main}
          style={{
            color: Colors.main.main,
            fontSize: 15,
          }}
          placeholder={'Search Contacts'}
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={(text) => {
            setSearchData(text)
            text.length === 0 ? textLength(false) : textLength(true)
          }}
        />
    </View>
  )
}

export default SearchContacts;
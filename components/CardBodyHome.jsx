import React, { useState, useContext, useEffect} from 'react';
import { View, Text, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import styles from '../styles/home';
import SkillList from '../components/Skills';
import Colors from '../constants/Colors';
import { AntDesign } from '@expo/vector-icons';
import AddSkillSet from './AddSkillSet';
import Fade from 'react-native-fade';
import { getSkills } from '../hooks/skillsHooks';
import { UserContext } from '../context/UserContext';

const { width, height } = Dimensions.get('window')


const CardBodyHome = () => {
  const [addSkill, setAddSkill] = useState(false);
  const [state,] = useContext(UserContext);
  const [skillList, setSkillList] = useState([]);

  useEffect(() => {
    fetchSkills();
  },[])

  const fetchSkills = async () => {
    const skillListArray = await getSkills(state);
    const { result } = skillListArray;
    setSkillList(result)
  }

  const setAddSkillProps = (e) => {
    setAddSkill(false);
    fetchSkills();
  }

  return (
    <View
      style={{
        paddingHorizontal: width * .07,
        marginVertical: 10,
      }}
    >
      <View style={{
        flexDirection: 'row',
        justifyContent: 'space-between'
      }}>
        <Text style={{ 
          color: Colors.main.main,
          fontWeight: 'bold',
          fontSize: 15
        }}>
            Skills and Certificates
        </Text>
        <TouchableOpacity 
          onPress={()=> {
            setAddSkill(!addSkill)
            fetchSkills();
          }}  
          style={{}}
        >
          { 
            addSkill ? <AntDesign name="minuscircleo" size={20} color={Colors.main.main} /> 
            : <AntDesign name="pluscircleo" size={20} color={Colors.main.main} />
          }
        </TouchableOpacity>
      </View>

      {/* <Fade visible={addSkill} direction="up"> */}
        {
          addSkill && <AddSkillSet setAddSkill={setAddSkillProps}/>
        }
      {/* </Fade> */}
      <View 
        style={{}}
      >
      { 
        !addSkill && 
          <FlatList
            keyExtractor={(item => item._id)}
            showsVerticalScrollIndicator={false}
            data={skillList}
            renderItem={({ item })=> {
              return <SkillList skills={item} />
            }}
          />
      }
      </View>
    </View>
  )
}

export default CardBodyHome;

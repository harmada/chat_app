import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import styles from '../styles/header';



const Header = (props) => {


  const [isFollowed, setIsFollowed] = useState(false)
  const { route } = props.user.props;
  const { navigation } = props.user.props;
  const { params } = route;


  
  return (
    <View 
    style={styles.container}>
      <View
        style={styles.leftContainer}
      >
        <TouchableOpacity 
            style={styles.backButton}
            onPress={() => {
              navigation.goBack()
            }}
          >
          <MaterialCommunityIcons name="step-backward" size={40} color="white" />        
        </TouchableOpacity>
      </View>
     
        <View style={styles.mainUserContainer}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Connection', params) }
          >
            <Image style={styles.avatar} size={50} source={{ uri: params.image }} />
          </TouchableOpacity>
          <Text 
            style={styles.textHeader}> { params.name } </Text>
          <Text style={{ color: 'white'}}>{ params.role }</Text>
          
        </View>
        <View
        
        >
         <TouchableOpacity
            style={{ borderColor: 'white' , borderWidth: 1, padding: 10, margin: 10, borderRadius: 20 }}
            onPress={()=> setIsFollowed(!isFollowed) }
          >
            
            <Text
              style={{ color: 'white'}}
            >
              { isFollowed ? 'Follow' : 'Unfollow' }
            </Text>
          </TouchableOpacity>
        </View>
    </View>
  )
}

export default Header;
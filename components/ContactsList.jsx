import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Image, Dimensions } from 'react-native';
import styles from '../styles/contact';
import { userStatus } from '../hooks/contactHooks';
import { FontAwesome } from '@expo/vector-icons'; 
import { searchCompanyProfile } from '../hooks/profileHooks';
import { useNavigation } from '@react-navigation/native';
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window')


const ContactList = (props) => {

  const { user } = props;
  const navigation = useNavigation();

  return (
    <View>
      <TouchableOpacity
        style={{
      
        }}
        onPress={()=>{
          const userDetails = { 
            _id: user._id,
            name: `${user.firstName} ${user.lastName}`, 
            image: user.image,
            status: user.status,
            email: user.email,
            jobTitle: user.jobTitle,
            companyName: user.company
            
          }
          navigation.navigate('Connection', userDetails);
        }}
      >
        <View style={{
          backgroundColor: 'white',
          padding: 20,
          borderRadius: 10,
          margin: 5,
          width: width * .4,
          height: height * .3,
          alignItems: 'center',
          justifyContent: 'center',
          shadowOpacity: .78,
          shadowRadius: 5,
          shadowColor: Colors.main.main,
          shadowOffset: { height: 5, width: -4 },
          elevation: 6
        }}>
          <View style={styles.leftContainer} >
            <Image 
              style={{ 
                height: height * .1,
                width: height * .1,
                borderRadius: height * .5,
                margin: 10,
                padding: 10,
                alignSelf: 'center',
                borderWidth: 2,
                borderColor: Colors.main.lines,
                
            }} 
            source={{uri: user.image}}
          />
          </View>
          <View style={{
            alignItems: 'center',

          }}>
            <Text style={{ 
              color: Colors.main.main
            }}>
            {user.firstName}
            </Text>
            <Text style={{
              fontSize: width * .03,
              color: Colors.main.main
            }}>
              {user.company}
            </Text>
            <Text style={{
              fontSize: width * .02,
              color: Colors.main.main
            }}>
              {user.email}
            </Text>
            <Text style={{
              fontSize: width * .02,
              color: Colors.main.main
            }}>
              {user.jobTitle}
            </Text>
          </View>
          
        </View>
      </TouchableOpacity>
      
    </View>
  )
}

export default ContactList;
import React, { useState, useEffect, useContext } from 'react';
import { View, Text, Dimensions } from 'react-native';
import styles from '../styles/chatMessage';
import Colors from '../constants/Colors';
import moment from "moment";
import { UserContext } from '../context/UserContext';
const { width, height } = Dimensions.get('window');


const ChatMessage = (props) => {


  const [userState,] = useContext(UserContext)
  const { message } = props;
  const isMyMessage = () => {
    return message.userId === userState._id;
  }
  return (
    <View style={[
      styles.messageBox, { 
        backgroundColor: isMyMessage() ? Colors.main.tertiary : Colors.main.main ,
        marginLeft: isMyMessage() ? 50 : 0,
        marginRight: isMyMessage() ? 0 : 50,
        shadowOpacity: .78,
        shadowRadius: 5,
        shadowColor: Colors.main.main,
        shadowOffset: { height: 5, width: -2 },
        elevation: 6
      } 
    ]}>
      <Text
        style={{ color:
          isMyMessage() ?  Colors.main.main : 'white',
        }}
      >{message.content}</Text>
      <View style={styles.timeStamps}>
        <Text
          style={{
            color: isMyMessage() ?  Colors.main.main : 'white',
            alignSelf: 'flex-end',
            fontSize: height * .015,
          }}
        >{moment(message.createdAt).fromNow()}</Text>
      </View>
    </View>

  )
}

export default ChatMessage;
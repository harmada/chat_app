import React, { useState, useContext } from 'react'
import { TextInput, Text, View, TouchableOpacity, FlatList, ScrollView, Alert, Dimensions } from 'react-native';
import styles from '../styles/skills'
import Colors from '../constants/Colors';
import { MaterialIcons } from '@expo/vector-icons'; 
import { searchIssuer, createSkills } from '../hooks/skillsHooks';
import CompanyTag from './CompanyTag';
import { Image } from 'react-native-elements';
import DatePicker from 'react-native-datepicker'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { UserContext } from '../context/UserContext';
const { width, height } = Dimensions.get('window');

const AddSkillSet = (props) =>  {
  const { setAddSkill } = props;
  const [state,] = useContext(UserContext)

  const skillLevel = [{ "level": 'Beginner' }, { "level": 'Intermediate' }, { "level": 'Advanced' }];
  
  const [skill, setSkill] = useState("")
  const [image, setImage] = useState()
  const [date, setDate] = useState("")
  const [issuerCompany, setIssuerCompany] = useState([]);
  const [issuerDomain, setIssuerDomain] = useState('');
  const [isClick, setIsClick] = useState(true);
  const [level, setLevel] = useState("")
  const [certId, setCertId] = useState("")

  const searchIssuerText = async (e) => {
    setIsClick(true);

    const issuer = await searchIssuer(e);
    const { data } = issuer;
    const { logo, domain } = data

    if (e.length === 1) {
      setIsClick(false);
      setImage('');
    }

    setImage(logo)
    setIssuerCompany(data)
    setIssuerDomain(domain)

    return createSkills();
  } 

  const setIssuer = (e) => {
    const { logo, domain, name } = e
    setIssuerCompany(name)
    setImage(logo);
    setIssuerDomain(domain)
    setIsClick(false)
  }

  const submitSkill = async () => {
    const { _id } = state;
    const skillDetails = {
      user_id: _id,
      skill,
      issuer: issuerCompany,
      issuerLogo: image,
      certificateId: certId,
      issuerCompany,
      issuerDomain,
      level,
      date
    }

    if (skillDetails.skill.length === 0) return Alert.alert('Error', 'Enter Valid Skill Set');
    const result = await createSkills(skillDetails)
    setAddSkill(false);
  }

  return (
    <ScrollView
      style={{}}
    > 
      <TextInput
        placeholderTextColor={Colors.main.main}
        style={{
          padding: 10,
          backgroundColor: Colors.main.background,
          color: Colors.main.main,
          borderRadius: 5,
          margin: 5,
          shadowColor: Colors.main.main,
          shadowRadius: 5,
          shadowOpacity: .5,
          shadowOffset: { height: 5, width: -2 },
          elevation: 6
        }}
        placeholder={"Skill/Certificate"}
        onChangeText={setSkill}
      />
      <View style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
        <TextInput
          placeholderTextColor={Colors.main.main}
          value={issuerCompany}
          style={{
            backgroundColor: Colors.main.background, 
            width: width * .7,
            margin: 5,
            padding: 10,
            shadowColor: Colors.main.main,
            shadowRadius: 5,
            shadowOpacity: .5,
            shadowOffset: { height: 5, width: -2 },
            borderRadius: 5,
            elevation: 6
          }}
          placeholder={"Issuer"}
          onChangeText={(e) => { searchIssuerText(e) }}
        />
        {
          image ? <Image
              style={{
                height: width * .1,
                width: width * .1,
                resizeMode: 'contain',
                shadowColor: Colors.main.main,
                shadowRadius: 5,
                shadowOpacity: .5,
                shadowOffset: { height: 5, width: -2 },
                borderRadius: 5,
                alignSelf: "center",
                elevation: 6
              }}
              source={{ uri: image }}
            /> : null
        }
      </View>
      {
        isClick && <FlatList 
          horizontal
          showsHorizontalScrollIndicator={false}
          styles={styles.issuerCompany}
          data={issuerCompany}
          keyExtractor={({item})=> item}
          renderItem={({ item })=> {
            return <CompanyTag company={item} setIssuer={setIssuer}  />
          }}
        />
      }
      <TextInput
        placeholderTextColor={Colors.main.main}
        style={{
          padding: 10,
          backgroundColor: Colors.main.background,
          color: Colors.main.main,
          borderRadius: 5,
          margin: 5,
          shadowColor: Colors.main.main,
          shadowRadius: 5,
          shadowOpacity: .5,
          shadowOffset: { height: 5, width: -2 },
          elevation: 6
        }}
        placeholder={"Certificate ID"}
        onChangeText={setCertId}
      />
      <View style={styles.skillLevelContainer}>
        <Text style={styles.textLevelHeader}>
          Level: 
        </Text>
        <FlatList 
          data={skillLevel}
          keyExtractor={({ item })=> item }
          renderItem={({ item })=> {
            return (
              <TouchableOpacity 
                style={[styles.skillLevelButton, item.level === level && { backgroundColor: Colors.main.main } ]}
                onPress={() => { setLevel(item.level) }}
              >
                <Text style={{ color: 'white', padding: 8 }}> {item.level} </Text> 
              </TouchableOpacity>
            )
          }}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
      <DatePicker
        placeholderTextColor={Colors.main.main}
        style={{
          alignSelf: 'center',
          width: width * .77,
          height: height * .08,
          fontFamily: "poppins-regular",
          padding: 5,
        }}
        date={date}
        mode="date"
        placeholder="Date Issued"
        format="YYYY-MM-DD"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: "absolute",
            left: 0,
            marginLeft: 0,
            paddingLeft: 4,
          },
          dateInput: {
            justifyContent: "center",
            borderColor: Colors.main.main,
            borderWidth: 1,
            alignItems: "center",
            borderRadius: 5,
            shadowColor: Colors.main.main,
            shadowRadius: 5,
            shadowOpacity: .5,
          },
        }}
        onDateChange={setDate}
      />
      <TouchableOpacity 
        style={{
          marginVertical: 10,
          backgroundColor: Colors.main.main,
          padding: 10,
          borderRadius: 5,
          justifyContent: 'center',
          flexDirection: 'row',
        }}
        onPress={()=> {submitSkill()}}
      >
        <Text
          style={styles.textButton}
        >
          Add Skill
        </Text>
        <MaterialIcons name="add-box" size={24} color="white" />
      </TouchableOpacity>
  </ScrollView>
  )
}

export default AddSkillSet;
import React, { useState, useContext, useEffect } from 'react'
import { View, Dimensions, Image, Text, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window');
import { AntDesign } from '@expo/vector-icons'; 
import { UserContext } from '../context/UserContext';
import { follow, isFollow, unfollow, followers } from '../hooks/followHooks';
import { createChatRoom } from '../hooks/chatHooks'
import { ContactContext } from '../context/ContactContext'

const ChatHeader = (props) => {
  const { userDetails } = props;
  const [userState,] = useContext(UserContext);
  const [contactState, contactDispatch] = useContext(ContactContext);
  const [isFollowed, setIsFollowed] = useState();
  const [connectionId, setConnectionId] = useState();
  const { route } = userDetails;
  const isMe = route.params._id === userState._id;

  useEffect(() => {
    isUserFollowed()
  },[])

  const generateChatRoomId = async () => {
    const payload = {
      chatRoomName: "Private Chat",
      type: "Personal",
      userId: userState._id,
      receiverId: route.params._id
    }

    const chat = await createChatRoom(payload)
    const { _id } = chat.data.chatRoom;
    
    const propsForChat = {
      _id: route.params._id,
      name: route.params.name,
      companyName: route.params.companyName,
      email: route.params.email,
      image: route.params.image,
      jobTitle: route.params.jobTitle,
      chatRoomId: _id
    }

    userDetails.navigation.navigate('ChatMessage', propsForChat) 

  }

  const isUserFollowed = async () => { 
    const payload = { 
      userId: userState._id,
      connectionId: route.params._id
    }

    const result = await isFollow(payload);
    if(result.length === 0) {
      return null;
    } 
    setIsFollowed(result[0].status)
    setConnectionId(result[0]._id)
  }

  const followUser = async () => {
    if(!isFollowed) {
      const payload = { 
        userId: userState._id,
        connectionId: route.params._id
      }
      await follow(payload);
      const contacts = await followers(userState._id);

      contactDispatch({ type: 'SAVE_DATA', payload: contacts.data.followers });
      return await isUserFollowed();
    }

    const payload = {
      _id : connectionId,
    }

    await unfollow(payload);
    const contacts = await followers(userState._id);

    contactDispatch({ type: 'DELETE_DATA', payload : route.params._id });
    return await isUserFollowed();
 
  }

  return (
    <View
      style={{
        backgroundColor: Colors.main.main,
        height: height * .40,
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Image
        style={{
          height: width * .25,
          width: width* .25,
          borderRadius: 60,
          borderWidth: 3,
          borderColor: Colors.main.lines,
          alignSelf: 'center',
        }}
        source={{ uri: route.params.image }}
      />
      <View
        style={{
          padding: 10,
          alignItems: 'center',
        }}
      >
        <Text
          style={{ color: Colors.main.lines, fontSize: 15, fontWeight: 'bold' }}
        >
          { route.params.name }
        </Text>
        <Text style={{ color: 'white' }}>
          { route.params.jobTitle }
        </Text>
        <Text style={{ color: 'white' }}>
          { route.params.email }
        </Text>
        <Text style={{ color: 'white', fontWeight: 'bold'}}>
          { route.params.companyName }
        </Text>
        { !isMe && 
          <View
            style={{
              flexDirection: 'row',
            }}
          >
            { 
              route.name === "Connection" && 
              <TouchableOpacity
                style={{ 
                  borderColor: Colors.main.lines , 
                  borderWidth: 1, 
                  padding: 10, 
                  marginVertical: 10, 
                  borderRadius: 10, 
                  justifyContent: 'center', 
                  alignItems: 'center', 
                  marginRight: 5
                }}
                onPress={()=> followUser() }
              >
                <Text
                  style={{ color: Colors.main.lines }}
                >
                  { isFollowed ? 'Unfollow' : 'Follow' }
                </Text>
            
              </TouchableOpacity>
            } 
            { 
              route.name === "Connection" && isFollowed ?
              <TouchableOpacity
                style={{ 
                  borderColor: Colors.main.lines, 
                  borderWidth: 1, 
                  padding: 5, 
                  marginVertical: 10, 
                  borderRadius: 10, 
                  alignItems: 'center', 
                  flexDirection: 'row'
                }}
                onPress={()=> {
                  generateChatRoomId()
                }}
              >
                <Text style={{ color: Colors.main.lines , padding: 5 }} >
                  Message
                </Text>
                <AntDesign name="message1" size={24} color={Colors.main.lines} />
              </TouchableOpacity>
              : null
            }
          </View>
        } 
      </View>
     
    </View>
  )
}
export default ChatHeader;
import React, { useContext, useState } from 'react';
import { ScrollView, Text, View, TextInput, TouchableOpacity, Image, Dimensions, Alert } from 'react-native';
import styles from '../styles/profile';
import Colors from '../constants/Colors';
import { Entypo, MaterialIcons, FontAwesome5 } from '@expo/vector-icons'; 
import PhoneInput from "react-native-phone-number-input";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
const { width, height } = Dimensions.get('window');
import { getDataFromToken } from '../hooks/authHooks';
import { UserContext } from '../context/UserContext';
import { CompanyContext } from '../context/CompanyContext';
import { updateProfile, getCompanyDetails } from '../hooks/profileHooks';


const Profile = () => {

  const [state, userDispatch] = useContext(UserContext);
  const [companyState, companyDispatch] = useContext(CompanyContext);

  const [phoneNumber, setPhoneNumber] = useState(state.mobileNumber);
  const [firstName, setName] = useState(state.firstName);
  const [lastName, setLastName] = useState(state.lastName);
  const [jobTitle, setJobTitle] = useState(state.jobTitle);
  const [company, setCompany] = useState(companyState.company);

  const submitProfile = async () => {
    const payload = {
      _id: state._id,
      firstName,
      lastName,
      jobTitle,
      company,
      mobileNumber: phoneNumber
    }

    const result = await updateProfile(payload);
    if(result.status === 200) {
      const data = await getDataFromToken();
      const companyData = await getCompanyDetails(data);
      userDispatch({ type: 'SAVE_DATA', payload: data })
      companyDispatch({ type: 'SAVE_DATA', payload: companyData })
      Alert.alert(`Hi ${firstName}`,'Sucessfully Update')
    }
        
  }

  return (
    <ScrollView>
    <View style={{ 
      width, 
      height,
      backgroundColor: Colors.main.dirtyWhite,
    }}>
      <View style={{
        alignItems: 'center',
      }}>
        <TextInput
          placeholderTextColor={Colors.main.main}
          defaultValue={firstName}
          style={{
            backgroundColor: Colors.main.background,
            height: height * .05,
            width: width * .8,
            padding: 10,
            margin: 5,
            borderRadius: 4,
            elevation: 5,
            color: Colors.main.main,
            shadowColor: Colors.main.main,
            shadowOpacity: .6,
            shadowOffset: { height: 5, width: -2 }
          }}
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Name"
          onChangeText={setName}
        />
        <TextInput
          defaultValue={lastName}
          style={{
            backgroundColor: Colors.main.background,
            height: height * .05,
            width: width * .8,
            padding: 10,
            margin: 5,
            borderRadius: 4,
            elevation: 5,
            color: Colors.main.main,
            shadowColor: Colors.main.main,
            shadowOpacity: .6,
            shadowOffset: { height: 5, width: -2 }
          }}
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Last Name"
            placeholderTextColor={Colors.main.main}
            onChangeText={setLastName}
        />
        <TextInput
          defaultValue={jobTitle}
          style={{
            backgroundColor: Colors.main.background,
            height: height * .05,
            width: width * .8,
            padding: 10,
            margin: 5,
            borderRadius: 4,
            elevation: 5,
            color: Colors.main.main,
            shadowColor: Colors.main.main,
            shadowOpacity: .6,
            shadowOffset: { height: 5, width: -2 }
          }}
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Job Title"
            placeholderTextColor="#fff"
            onChangeText={setJobTitle}
        />
        <TextInput
          value={company}
          style={{
            backgroundColor: Colors.main.background,
            height: height * .05,
            width: width * .8,
            padding: 10,
            margin: 5,
            borderRadius: 4,
            elevation: 5,
            color: Colors.main.main,
            shadowColor: Colors.main.main,
            shadowOpacity: .6,
            shadowOffset: { height: 5, width: -2 }
          }}
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Company"
          placeholderTextColor="#fff"
          onChangeText={setCompany}
          />

        <PhoneInput
          defaultValue={phoneNumber}
          defaultCode="PH"
          onChangeText={setPhoneNumber}
          containerStyle={{
            alignItems: 'center',
            borderRadius: 10,
            justifyContent: 'center',
            backgroundColor: Colors.main.background,
            shadowColor: Colors.main.main,
            shadowOpacity: .6,
            shadowOffset: { height: 5, width: -2 },
            elevation: 5,
          }}
          textInputProps={{
            bordeRadius: 10,
            maxLength: 10,
            alignItems: 'center',
            justifyContent: 'center',
            height: height * .05,
            width: width * .8,
            placeholder: "Mobile Number",
            placeholderTextColor: Colors.main.main,
          }}
        />
        <TouchableOpacity
          style={{ 
            backgroundColor: Colors.main.main,
            marginTop: 10,
            borderRadius: 10, 
            height : height * .06,
            width: width * .8,
            padding: 10, 
            alignItems: 'center', 
            justifyContent: 'center',
            flexDirection: 'row',
          
          }}
          onPress={submitProfile}
        >
          <Text
            style={{ color: Colors.main.lines, fontWeight: "bold",  paddingHorizontal: 10, fontSize: 15 }}
          >
            Update 
          </Text>
          <MaterialIcons name="system-update-tv" size={24} color={Colors.main.lines} style={styles.icon} />
        </TouchableOpacity>
      </View>
        
    </View>
  </ScrollView>

  )
}


export default Profile;
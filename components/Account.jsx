import React, { useState, useContext, useEffect } from 'react'
import { View, Dimensions, TextInput, TouchableOpacity, Text } from 'react-native';
import Colors from '../constants/Colors';
import { Entypo, MaterialIcons, FontAwesome5 } from '@expo/vector-icons'; 
import styles from '../styles/profile';
import { Alert } from 'react-native';
import validator from 'validator';
import { UserContext } from '../context/UserContext';
import { updateAccount } from '../hooks/authHooks';
const { width, height } = Dimensions.get('window');

const Account = () => {

  const [state,] = useContext(UserContext)

  const [email, setEmail] = useState(state.email)
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const submitAccount = async () => {
    const payload = {
      _id: state._id,
      email,
      password
    }

    if(!validator.isEmail(email)) {
      return Alert.alert('Email is not valid')
    }

    if(password !== confirmPassword) {
      return Alert.alert('Password not match')
    }

    const result = await updateAccount(payload)
    if(result.status === 200) {
      return Alert.alert(`Hi ${state.firstName}`, 'Your Account has been updated');
    }

  }


  return (
    <View 
      style={{ 
        padding: 20, 
        backgroundColor: Colors.main.dirtyWhite, 
        width, 
        height
        }}
      >
        <View
          style={{
            alignItems: 'center',
          }}
        >
          <TextInput
          onChangeText={setEmail}
          defaultValue={email}
          style={styles.inputStyle}
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Email"
            placeholderTextColor={Colors.main.main}
          />
          <TextInput
            style={styles.inputStyle}
              onChangeText={setPassword}
              autoCorrect={false}
              secureTextEntry
              autoCapitalize="none"
              placeholder="Password"
              placeholderTextColor={Colors.main.main}
            />
          <TextInput
            style={styles.inputStyle}
              autoCorrect={false}
              secureTextEntry
              autoCapitalize="none"
              placeholder="Confirm Password"
              placeholderTextColor={Colors.main.main}
              onChangeText={setConfirmPassword}
            />
          </View>
          <View
            style={{
              padding: 20,
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}
          >
            <TouchableOpacity
              style={{ 
                marginVertical: 10,
                backgroundColor: Colors.main.main, 
                borderRadius: 10, 
                width: width * .8,
                padding: 10, 
                alignItems: 'center', 
                justifyContent: 'center',
                flexDirection: 'row',
              }}
              onPress={ submitAccount }
            > 
              <Text
                style={{ 
                  color: Colors.main.lines, 
                  fontWeight: "bold",  
                  paddingHorizontal: 10 }}
              >
                Update 
              </Text>
              <MaterialIcons name="system-update-tv" size={24} color={Colors.main.lines} style={styles.icon} />
            </TouchableOpacity>
      </View>
    
    </View>
  
  )
}
export default Account

import React, { useState, useContext } from 'react'
import { View, TouchableOpacity, Text, TextInput, Image, Dimensions } from 'react-native';
import { FontAwesome, Entypo } from '@expo/vector-icons';
import styles from '../styles/auth';
import { loginAuth } from '../hooks/authHooks';
import * as RootNavigation from '../RootNavigation';
import { UserContext } from '../context/UserContext';
import validator from 'validator';
import { logo } from '../constants/Image';
import { useNavigation } from '@react-navigation/native';
const { width, height } = Dimensions.get('window');
import Colors from '../constants/Colors'
const AuthCard = () => {


  const navigation = useNavigation();
  const [, dispatch] = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState({
    username: false,
    password: false
  })

  const setErrorValidation = ({ email }) => {
    setError({
      ...error,
      email: !validator.isEmail(email),
      password: password.length === 0,
    });
  }

  const login = async () => {
    const credentials = {
      email,
      password
    }

    setErrorValidation(credentials);
    const result = await loginAuth(credentials)
    dispatch({ type: "SAVE_DATA", payload: result });
    
    if(result === undefined) {
      return
    }
    navigation.navigate("Home")
  }

  const Register = () => {
    RootNavigation.navigate("Register");
  }

  return (
    <View style={{
      backgroundColor: "#e9e9e9",
      width, 
      height,
      paddingHorizontal: width * .02,
      paddingVertical: height * .02,
      justifyContent: "center",
    }}>
      <View style={{
        alignItems: 'center',
        padding: 20
      }}>
        <Image
          source={{ uri: logo }}
          style={{
            height: height * .13,
            width: width * 2,
            resizeMode: 'contain'
          }}
        />
      </View>
      <View style={{
        padding: 14,
        margin: 20,
        // backgroundColor: 'white',
        borderRadius: 10,
    
      }}>
        <View style={{
          padding: 4
        }}>
          <Text style={{
            fontSize: 20,
            paddingVertical: 4,
            color: Colors.main.main,
            fontWeight: 'bold'
          }}>
            Sign In
          </Text>
          <Text
            style={{
              paddingVertical: 4,
              color: Colors.main.main
            }}
          >
            Login with your account
          </Text>
        </View>
        <View style={{}}>
          <TextInput
            style={[styles.textInput, error.email && { borderBottomColor: "red" }]}
            placeholder={"Username"}
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={setEmail}
          />
        </View>
        {
          error.email && (<Text style={{ color: 'red' }}> Email is not valid</Text>)
        }
        <View style={{}}>
          <TextInput
            style={[styles.textInput, error.password && { borderBottomColor: "red" }]}
            placeholder={"Password"}
            secureTextEntry
            autoCorrect={false}
            autoCapitalize="none"
            onChangeText={setPassword}
          />
        </View>

        {
          error.password && (<Text style={{ color: 'red' }}> Password cannot be empty </Text>)
        }
        <View style={styles.register} >
          <TouchableOpacity
            onPress={() => {
              RootNavigation.navigate('ForgotPassword')
            }}
            style={{ 
              padding: 20
            }}
          >
            <Text style={{
              color: Colors.main.main
            }}>Forgot Password? Click here</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 1, height: 1, backgroundColor: Colors.main.main }} />
          <View>
            <Text style={{ width: 50, textAlign: 'center' }}> . </Text>
          </View>
          <View style={{ flex: 1, height: 1, backgroundColor: Colors.main.main }} />
        </View>
        <View style={styles.register} >
          <TouchableOpacity
            onPress={() => {
              Register();
            }}
            style={{ 
              padding: 20
            }}
          >
            <Text style={{
              color: Colors.main.main
            }}>  No account? Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => {
          login();
        }}
        style={{
          backgroundColor: Colors.main.main,
          width: width * .75,
          height: height* .06,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          borderRadius: 10,
        }}
      >
        <Text style={{
          color: 'white',
          marginHorizontal: 10
        }}>
          Sign In
        </Text>
        <FontAwesome name="sign-in" size={24} color="white" />
      </TouchableOpacity>
    </View>
  )
}

export default AuthCard;
import React, { useContext, useEffect, useState } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import styles from '../styles/home';
import { UserContext } from '../context/UserContext';
import { CompanyContext } from '../context/CompanyContext';
import { ContactContext } from '../context/ContactContext';
import * as RootNavigation from '../RootNavigation';
import Colors from '../constants/Colors';

const CardProfile = () => {

  const [companyState,] = useContext(CompanyContext)
  const [userState,] = useContext(UserContext)
  const [contactState,] = useContext(ContactContext)

  return (
    <View
      style={styles.cardProfile}
    >
      <View style={styles.cardHeader}>
        <TouchableOpacity
          onPress={()=> { RootNavigation.navigate('Profile')}}
        >
          { userState.image 
            ? <Image style={styles.image} source={{ uri: userState.image }} />
            : <Image style={styles.image} source={ require('../assets/male-placeholder.png') } /> 
          }
        </TouchableOpacity>

      </View>
      <View style={{
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center',
        
      }}>
        <Text style={styles.name}>
            {`${userState.firstName} ${userState.lastName}`} 
        </Text>
        <Text style={{
          color: Colors.main.main,
          fontWeight: 'bold',
        }}>
          { userState.jobTitle }
        </Text>
        <Text style={styles.subText}>
          { companyState.company }
        </Text>
        <Text style={styles.subText}>
          { userState.email }
        </Text>
        <TouchableOpacity
          style={{
            backgroundColor: Colors.main.main,
            padding: 5,
            marginVertical: 3,
            borderRadius: 5,
            alignItems: 'center'
          }}
          onPress={() => {
            RootNavigation.navigate('Contact')
          }}
        >
          <Text style={{ color: Colors.main.background }}>
            {` ${Object.values(contactState).length} Connections` } 
          </Text>
        </TouchableOpacity>

      </View>
  </View>
    )
}

export default CardProfile;

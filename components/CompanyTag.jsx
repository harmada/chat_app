import React from 'react'
import { View, TouchableOpacity, Text } from 'react-native';
import { Image } from 'react-native-elements';
import Colors from '../constants/Colors';
import styles from '../styles/skills'
const CompanyTag = (props) => {
  const { company, setIssuer } = props;

  return (
    <TouchableOpacity
      style={{
        backgroundColor: Colors.main.main,
        padding: 4,
        margin: 4,
        borderRadius: 4,
        flexDirection: 'row',
      }}
      onPress={(e) => { setIssuer(company) }}
    >
      <Text
        style={{
          color: 'white',
          padding: 4,
          marginRight: 5,
        }}
      >
        { company.name }
      </Text>
      
      <Image
        style={styles.imageSize}
        source={{ uri: company?.logo }}
      />
    </TouchableOpacity>
  )
}
export default CompanyTag;
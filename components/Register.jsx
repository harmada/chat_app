import React, { useState } from 'react'
import { ScrollView, View, TouchableOpacity, Text, TextInput, Dimensions, Image, Alert } from 'react-native';
import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons'; 
import styles from '../styles/register';
import { register, getCompanyProfile } from '../hooks/authHooks';
import PhoneInput from "react-native-phone-number-input";
import  moment from 'moment';
const { width, height } = Dimensions.get('window');
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Colors from '../constants/Colors';
import validator from 'validator';
import DatePicker from 'react-native-datepicker';
import * as RootNavigation from '../RootNavigation';

const RegisterCard = () => {


  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [birthday, setBirthday] = useState("");
  const [jobTitle, setJobTitle] = useState("");
  const [company, setCompany] = useState("");

  const [error, setError] = useState({
    username: false,
    password: false,
    birthday: false,
    jobTitle: false,
    company: false,
    number: false,
  });

  const setErrorValidation = ({ email }) => {
    
    const today = new Date();

    var start = moment((today), "YYYY-MM-DD");

    setError({...error, 
      name:         name.length === 0,
      lastName:     name.length === 0,
      username:     !validator.isEmail(email),
      password:     password !== confirmPassword,
      nullPassword: password.length === 0,
      jobTitle:     jobTitle.length === 0,
      company:      company.length === 0,
      number:       phoneNumber.length === 0,
      birthday:     start.diff(birthday, 'days') < 0
    });
  }

  const registerUser = async () => {
    const credentials = {
      name,
      lastName,
      email: username,
      password,
      phoneNumber,
      birthday,
      jobTitle,
      company
    }
    
    setErrorValidation(credentials);

    if(
      name.length === 0,
      lastName.length === 0,
      !validator.isEmail(username) || 
      password.length === 0 ||
      password !== confirmPassword ||
      jobTitle.length === 0 || 
      company.length === 0 || 
      phoneNumber.length === 0 ||
      birthday.length === 0
    )  
    { 
      return Alert.alert('Please fill up the fields');
    }
    
    const companyDetails =  await getCompanyProfile(credentials);
    const  { _id } = companyDetails;
    const userDetails = {
      firstName: name,
      lastName,
      email: username,
      password,
      mobileNumber: `0${phoneNumber}`,
      birthDate: birthday,
      jobTitle,
      company: _id,
      accessType: "User"
    }
    register(userDetails)
    RootNavigation.navigate('Auth');
  }

  return (
    <ScrollView 
      style={{
        height,
        width,
        backgroundColor: Colors.main.dirtyWhite
      }}
    >
      <View
        style={{
          padding: 20,
          margin: 10
        }}
      >
        <View 
          style={{
            width: width * .4
          }}
        >
          <Text 
            style={{
              fontSize: 40,
              fontFamily: "Cochin",
              paddingVertical: 20,
              color: Colors.main.main
            }}>
              Hello! Sign Up to get Started!
          </Text>
        </View>
      <View 
        style={{
          alignItems: 'center',
          margin: 10,
          paddingVertical: 5,
        }}
      >
        <TextInput
          style={styles.textInput}
          placeholder={"Name"}
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={setName}
        />
        <TextInput
          style={styles.textInput}
          placeholder={"Last Name"}
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={setLastName}
        />
        <TextInput
          style={[
            styles.textInput, 
            error.username && { borderBottomColor: "red" }  ]}
          value={username}
          placeholder={"Email address"}
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={setUsername}
        />
        {
          error.username && (<Text style={{ color: 'red' }}> Email is not valid</Text>)
        }
        <TextInput
          style={[
            styles.textInput, 
            error.nullPassword && { borderBottomColor: "red" } ,
            error.password && { borderBottomColor: "red" } 
          ]}
          value={password}
          placeholder={"Password"}
          secureTextEntry
          autoCorrect={false}
          autoCapitalize="none"
          onChangeText={setPassword}
        />
        {
          error.nullPassword && (<Text style={{ color: 'red' }}> Password cannot be empty </Text>)
        }
        {password.length !==0 && 
          <TextInput
            style={[styles.textInput, error.password && { borderBottomColor: "red"} ]}
            value={confirmPassword}
            placeholder={"Confirm Password"}
            secureTextEntry
            autoCorrect={false}
            autoCapitalize="none"
            onChangeText={setConfirmPassword}
          />
        }
        {
          error.password && (<Text style={{ color: 'red' }}> Password do not match </Text>)
        }
        <TextInput
          style={[styles.textInput, error.jobTitle && { borderBottomColor: "red"} ]}
          placeholder={"Enter Job Title"}
          autoCorrect={false}
          autoCapitalize="none"
          onChangeText={setJobTitle}
        />
        {
          error.jobTitle && (<Text style={{ color: 'red' }}> Enter a job title </Text>)
        }
        <TextInput
          style={[styles.textInput, error.company && { borderBottomColor: "red"} ]}
          placeholder={"Enter Company Name"}
          autoCorrect={false}
          autoCapitalize="none"
          onChangeText={setCompany}
        />
        {
          error.company && (<Text style={{ color: 'red' }}> Enter a company or indicate if Freelance status </Text>)
        }
        <PhoneInput
          defaultValue={phoneNumber}
          defaultCode="PH"
          onChangeText={setPhoneNumber}
          containerStyle={{
            margin: 10,
            alignItems: 'center',
            borderRadius: 5,
            justifyContent: 'center',
            shadowColor: Colors.main.main,
            shadowOpacity: .6,
            shadowOffset: { height: 5, width: -2 },
            elevation: 5,
          }}
          textInputProps={{
            borderRadius: 5,
            bordeRadius: 10,
            maxLength: 10,
            alignItems: 'center',
            justifyContent: 'center',
            height: height * .05,
            width: width * .8,
            placeholder: "Mobile Number",
            placeholderTextColor: Colors.main.main,
          }}
        />
        {
          error.number && (<Text style={{ color: 'red', paddingBottom: 20 }}> Enter a Valid Number </Text>)
        }
        <DatePicker
          style={{
            width: wp("77%"),
            height: hp("8%"),
            fontFamily: "poppins-regular",
          }}
          date={birthday}
          mode="date"
          placeholder="Birth Date"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: "absolute",
              left: 0,
              marginLeft: 0,
              paddingLeft: 4,
            },
            dateInput: {
              justifyContent: "center",
              borderColor: Colors.main.main,
              borderWidth: 1,
              alignItems: "center",
              borderRadius: 5,
              shadowColor: Colors.main.main,
              shadowRadius: 5,
              shadowOpacity: .5,
            },
          }}
          onDateChange={setBirthday}
        />
        {
          error.number && (<Text style={{ color: 'red', paddingBottom: 20 }}> Enter a Valid Date </Text>)
        }

        <View style={styles.register} >
          <TouchableOpacity
            onPress={() => {
              RootNavigation.navigate('Auth')
            }}
            style={{ 
              padding: 20
            }}
          >
            <Text style={{
              color: Colors.main.main
            }}>  No account? Click Here to Register</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity 
          onPress={()=> {
            registerUser();
          }}        
          style={{
            width: width * .8,
            backgroundColor: Colors.main.main,
            height: height * .065,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}
        >
          <Text style={styles.textButton}>
            Sign In
          </Text>
          <FontAwesome name="sign-in" size={24} color="white" />
        </TouchableOpacity>
      </View>
      </View>
    </ScrollView>
  )
}

export default RegisterCard;
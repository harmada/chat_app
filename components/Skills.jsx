import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import styles  from '../styles/skills';
import Colors from '../constants/Colors';
import { Image } from 'react-native-elements';
const { width, height } = Dimensions.get('window')
const SkillList = ({ skills }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        paddingHorizontal: 10,
        marginTop: 10,
      }}
    > 
      <Image
        style={{ 
          width: width * .20,
          height: width * .20,
          padding: 10,
          borderRadius: 10,
          marginVertical: 10,
          marginRight: 10,
        }}
        source={{ uri: skills.issuerLogo }}
      />
      <View 
        style={{ 
          justifyContent: 'center' 
        }
      }>
        <Text 
          style={{
          color: Colors.main.main,
          fontWeight: 'bold'
          }}
        >
          { skills.skill }
        </Text>
        { skills.issuerDomain && <Text style={{
          color: Colors.main.main
        }}>{ skills.issuerDomain }</Text> }
        { skills.issuer && <Text style={{ color: Colors.main.main }}>{ skills.issuer }</Text> }
        { skills.issuedAt && <Text style={{ color: Colors.main.main }}>Issued At: { skills.issuedAt }</Text> }
        { skills.level && <Text style={{ color: Colors.main.main }}>{ skills.level }</Text> }
        { skills.certificateId !== "" && <Text style={{ color: Colors.main.main }}>Certificate ID: { skills.certificateId  }</Text> }
      </View>
    </View>
  )
}

export default SkillList;
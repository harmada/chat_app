export default [{
    id: 'u1',
    name: 'Vadim',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/1.jpg',
    status: "Busy",
    role: "Senior Engineer",
    company: "Xypher Solutions Inc.",
    email: 'vadim@xypher.com'
  }, {
    id: 'u2',
    name: 'Lukas',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/2.jpg',
    status: "Online",
    role: "UX/UI Engineer",
    company: "Xypher Solutions Inc.",
    email: 'lucas@xypher.com'
  }, {
    id: 'u3',
    name: 'Daniil',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/3.jpg',
    status: "Busy",
    role: "UX/UI Engineer",
    company: "Xypher Solutions Inc.",
    email: 'danill@xypher.com'
  }, {
    id: 'u4',
    name: 'Alex',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/3.png',
    status: "Busy",
    role: "Sofware Engineer",
    company: "Xypher Solutions Inc.",
    email: 'alex@xypher.com'
  }, {
    id: 'u5',
    name: 'Vlad',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/4.jpg',
    status: "Online",
    role: "DevOps Engineer",
    company: "MyHype IT Solutions Inc.",
    email: 'vlad@myhype.com'
  }, {
    id: 'u6',
    name: 'Elon Musk',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/5.jpg',
    status: "Busy",
    role: "DevOps Engineer",
    company: "MyHype IT Solutions Inc.",
    email: 'elon@myhype.com'
  }, {
    id: 'u7',
    name: 'Adrian',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/6.png',
    status: "Offline",
    role: "DevOps Engineer",
    company: "MyHype IT Solutions Inc.",
    email: 'adrian@myhype.com'
  }, {
    id: 'u8',
    name: 'Borja',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/7.png',
    status: "Offline",
    role: "Software Engineer",
    company: "Cloud Panda Solutions",
    email: 'borja@cp.com'
  }, {
    id: 'u9',
    name: 'Mom',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/8.png',
    status: "Online",
    role: "Junior Developer",
    company: "Cloud Panda Solutions",
    email: 'mom@cp.com'
  }, {
    id: 'u10',
    name: 'Angelina Jolie',
    imageUri: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/avatars/9.png',
    status: "Online",
    role: "Junior Developer",
    company: "Cloud Panda Solutions",
    email: 'angel@cp.com'
  }]
  
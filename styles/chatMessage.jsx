import React from 'react';
import { StyleSheet, Dimensions} from "react-native";
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
  },
  mainContainer: {
    // padding: 10,
    // margin: 10,
    // paddingHorizontal: 10,
    // marginHorizontal: 50,
    // alignSelf: 'center'
  },
  messageBox: {
    alignSelf: 'center',
    width: width * .7,
    padding: 10,
    margin: 5,
    borderRadius: 10
  },
  timeStamps: {
    flexDirection: 'row',
    
    justifyContent: 'flex-end',
  }
});

export default styles;
import React from 'react';
import { StyleSheet } from 'react-native';
import Colors from '../constants/Colors';

const navStyles = StyleSheet.create({
  icon: {
    padding: 10,
  },
  iconContainer: {
    flexDirection: 'row',
  },
  headerImage: {
    width: 20,
    height: 20,
    borderRadius: 25,
    alignSelf: 'center',
  }
})

export default navStyles;
import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../constants/Colors';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  icon: {
    padding: 10,
  },
  container: {
    flex: 1
  },
  authHeader: {
    paddingHorizontal: 30,
    margin: 10,
  },
  authTextHeader: {
    fontSize: 30,
    marginVertical: 5,
    marginTop: 40
  },
  mainContainer: {
    borderRadius: 5,
    backgroundColor: Colors.light.background,
    padding: 20,
    margin: 20,
    alignItems: "center",

  },
  textInput: {
    backgroundColor: Colors.main.background,
    height: height * .06,
    width: width * .8,
    borderRadius: 5,
    paddingHorizontal: 10,
    color: Colors.main.main,
    marginVertical: 3,
    elevation: 6,
    shadowOffset: { height: 5, width: -2 },
    shadowColor: Colors.main.main,
    shadowOpacity: .6,
  },  
  button: {
    flexDirection: 'row',
    backgroundColor: Colors.main.main,
    width: 150,
    alignSelf: 'flex-end',
    height: 50,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginRight: 5,
    marginTop: 20,
  },
  textButton: {
    color: Colors.light.background,
    fontWeight: 'bold',
    paddingHorizontal: 10
  },
  register: {
    marginTop: 5,
    alignItems: 'center',
  },
  registerText: {
    color: 'grey'
  }
})

export default styles;
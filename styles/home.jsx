import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
    },
    addSkill: {
      flexDirection: 'row',
      width: wp('25%'),
      marginTop: 13,
      marginHorizontal: 30,
      justifyContent: 'flex-end'
    },
    headerStyle:{
      flexDirection: 'row',
      padding: 5
    },
    connections: {
      padding: 2,
      borderWidth: 1,
      marginTop: 5,
      alignItems: 'center',
      borderColor: Colors.main.main,
      borderRadius: 4,
      width: wp("30%"),
    },  
    cardProfile: {
      flexDirection: 'row',
      backgroundColor: Colors.light.background,
      padding: 10,
      borderRadius: 10,
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: wp('5%'),
      marginVertical: 10,
      shadowOpacity: .78,
      shadowRadius: 5,
      shadowColor: Colors.main.main,
      shadowOffset: { height: 5, width: -4 },
      elevation: 6
    },
    cardBodyProfile: {
      flex: 1,
      padding: 5,
      borderRadius: 5,
      marginHorizontal: wp('5%'),
      marginBottom: hp('2%'),
    },
    cardHeader: {
    },
    image:{ 
      height: height * .12,
      width: height* .12,
      borderRadius: height * .06,
      margin: 5,
      borderColor: Colors.main.lines,
      borderWidth: 2,
    },
    subText: {
      fontSize: 12,
      paddingHorizontal: 5,
      color: Colors.main.main,
    },
    name: {
      paddingHorizontal: 5,
      fontSize: width * .04,
      fontWeight: 'bold',
      color: Colors.main.main,
    },
    bodyHeader: {
      paddingVertical: 10,
      paddingHorizontal: 5,
      fontSize: 15,
      fontWeight: 'bold',
      color: Colors.main.main,
    }
  });

  export default styles;

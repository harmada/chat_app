import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window');
const navStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: width - 20,
    borderRadius: 10,
    margin: 5,
  },
  mainContainer: {
    justifyContent: 'space-around',
  },
  leftContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,  
  },
  image: {
    justifyContent: 'center',
    height: 60,
    width: 60,
    borderRadius: 40,
  },
  nameLabel: {
    fontSize: 15,
  },
  roleLabel: {
    fontSize: 15,
    color:"grey"
  },
  status: {
    flexDirection: 'row',
    borderRadius: 50,
  },
  circle: {
    paddingRight: 5,
    alignSelf: 'center'
  },
  searchContainer: {
    flexDirection: 'row',
    padding: 10,
    margin: 10,
    backgroundColor: "#e5e5e5",
    borderRadius: 5
  }, 
  icon: {
    padding: 5
  },
  flatList: {
    flex: 1
  }
})

export default navStyles;
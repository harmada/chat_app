import React from 'react';
import Colors from '../constants/Colors';
import { StyleSheet } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
  

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderColor: Colors.main.main,
    shadowOpacity: .78,
    shadowRadius: 5,
    shadowColor: Colors.main.main,
    shadowOffset: { height: 5, width: -4 },
    elevation: 6
  },
  companyContainer: { 
    flexDirection: 'row' 
  },
  imageSkillsLogo: {
    width: 30,
    borderRadius: 10,
    resizeMode: 'contain',
  },  
  imageSize: { 
    height: 30, 
    width: 30,
    padding: 10,
    resizeMode: 'contain',
    borderRadius: 3
  },
  skillLevelButton: { 
    paddingHorizontal: 5, 
    marginHorizontal: 5, 
    backgroundColor: Colors.main.secondary, 
    borderRadius: 5
  },
  skillLevelContainer: { 
    flexDirection: 'row', 
    paddingHorizontal: 10, 
    paddingVertical: 15, 
    alignItems: 'center' 
  },
  textLevelHeader: { 
    color: 'grey', 
    paddingHorizontal: 10 
  },
  issuerCompany: { 
    padding: 20, 
    marginHorizontal: 50,
    height: 50
  },
  header: { 
    fontSize: 15, 
    fontWeight: 'bold', 
    color: '#464646', 
    padding: 10
  },
  textHeader: {
    fontSize: 15,
    paddingVertical: 1,
    fontWeight: 'bold',
    color: Colors.main.main,
  },
  addSkill: {
    marginBottom: 10,
    padding: 5,
    borderRadius: 5,
  },
  textInput: {
    paddingBottom: 5,
    marginBottom: 10,
    marginHorizontal: 20,
    borderBottomColor: "black",
    height: 30,
    borderBottomWidth: 1,
    borderBottomColor: 'grey'
  },
  textInputIssuer: {
    paddingBottom: 5,
    width: wp("60"),
    marginBottom: 10,
    marginHorizontal: 20,
    borderBottomColor: "black",
    height: 30,
    borderBottomWidth: 1,
    borderBottomColor: 'grey'
  },
  textButton: { 
    color: 'white', 
    padding: 5, 
    fontWeight: 'bold', 
  }
});

export default styles;
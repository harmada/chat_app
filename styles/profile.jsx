import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.main.main,
    padding: 20,
  },  
  tabStyle: {
    padding: 20,
    backgroundColor: Colors.main.main,
    alignSelf: 'center',
    width: 150,
    alignItems: 'center',
  },
  imageLogo: {
    height: 140,
    width: 140,
    borderRadius: 50
  },
  inputContainer: {
    marginTop: 10,
    marginHorizontal: 30,
  },  
  textInputContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#fff',
    paddingVertical: 10,
  },
  icon: {
    alignSelf: 'center',
  },
  inputStyle: {
    backgroundColor: Colors.main.background,
    height: height * .05,
    width: width * .8,
    padding: 10,
    margin: 5,
    borderRadius: 4,
    elevation: 5,
    color: Colors.main.main,
    shadowColor: Colors.main.main,
    shadowOpacity: .6,
    shadowOffset: { height: 5, width: -2 }
  },  
})

export default styles;
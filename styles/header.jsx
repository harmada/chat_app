import React from 'react';
import { StyleSheet } from 'react-native'
import Colors from '../constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
const styles = StyleSheet.create({
  container: {
    padding: 5,
    backgroundColor: Colors.main.main,
  },
  leftContainer: {
    justifyContent: 'center',
  },
  mainUserContainer: {
    alignItems: 'center',
    padding: 5,
  },
  textHeader:{ 
    paddingTop: 20,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white'
  },
  avatar: { 
    marginTop : 30,
    height: 70,
    width: 70,
    borderRadius: 20,
    alignSelf: 'center',
    padding: 20,
    resizeMode: 'contain',
  },
  backButton: {
    
  },
  backText: {
    fontSize: 20,
    color: Colors.light.background,
  },
  subText: {
    color: Colors.light.background,
  },
  subContainer: {

  }
});

export default styles;
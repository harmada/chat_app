import React from 'react';
import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center'    
  },
  image: {
    height: 50,
    width: 50,
    borderRadius: 20
  }
  
});
import React from 'react';
import { StyleSheet, Dimensions } from "react-native";
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 4,
    justifyContent: 'center',
    alignItems: 'center',
    width,
    bottom: 0,
  },
  mainContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingHorizontal: 10,
    borderRadius: 20,
    marginHorizontal: 5,
    marginVertical: 10,
    height: height * .07,
    width: width * .8,
    alignItems: 'center',
  },
  buttonContainer: {
    backgroundColor: Colors.main.main,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25
  },
  textInput: {
    marginHorizontal: 5,
    flex: 1,
  },
  icon: {
    paddingHorizontal: 10
  }
})


export default styles;
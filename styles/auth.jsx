import React from 'react';
import { StyleSheet,  Dimensions } from 'react-native';
import Colors from '../constants/Colors';
const { width, height } = Dimensions.get('window')
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  icon: {
    padding: 10,
  },
  iconTextInput: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    justifyContent: 'center',
    flex: 1
  },
  logo: { 
    height: 100, 
    resizeMode: 'contain', 
    padding: 20, 
    margin: 20 
  },
  authHeader: {
    paddingHorizontal: 30,
    margin: 10,
  },
  authTextHeader: {
    fontSize: 30,
    marginVertical: wp("3%"),
  },
  mainContainer: {
    alignItems: 'center',
    height: hp("20%"),
    padding: 20,
    borderRadius: 20,
  },
  textInputContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#000',
    paddingBottom: 5,
  },
  textInput: {
    height: height * .06,
    width: width * .75,
    paddingVertical: 5,
    backgroundColor: 'white',
    padding: 10,
    margin: 5,
    borderRadius: 10,
    color: Colors.main.main,
    shadowOpacity: .78,
    shadowRadius: 5,
    shadowColor: Colors.main.main,
    shadowOffset: { height: 5, width: -2 },
    elevation: 6
  },  
  button: {
    flexDirection: 'row',
    backgroundColor: Colors.main.main,
    width: wp("30%"),
    alignSelf: 'flex-end',
    height: hp('6%'),
    marginTop: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginRight: 20
  },
  textButton: {
    color: Colors.light.background,
    fontWeight: 'bold',
    paddingHorizontal: 10
  },
  register: {
    marginVertical: 10,
    alignItems: 'center',
  },
  registerText: {
    color: 'grey'
  }
})

export default styles;